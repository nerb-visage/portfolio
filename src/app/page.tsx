import { About, Experience, Technologies } from "@/components/Contents";
import { Projects } from "@/components/Project";
import SideBar, { SocialLink } from "@/components/SideLink";
import { SpotlightHover } from "@/components/Spotlight";
import Head from "next/head";
import Image from "next/image";

export default function Home() {
	return (
		<main className="md:min-h-[900px] h-screen flex">
			<SpotlightHover>
				<div className="top-0 left-0 right-0 p-4 md:p-10 z-50 relative">
					<div className="mx-auto flex flex-col items-center">
						<div className="font-extrabold text-white text-justify w-fit">
							<div className="text-2xl lg:text-5xl flex justify-between items-center">
								<span>Bren</span>
								<span className="h-2 w-2 lg:h-3 lg:w-3 rounded-full bg-gray-200"></span>
								<span>Steven</span>
							</div>
							<div className="text-5xl lg:text-8xl">JAVIER</div>
						</div>
						<div className="flex gap-x-4 my-4 flex-wrap justify-center">
							<SocialLink
								href="mailto:brensteven.javier@gmail.com"
								image="/icons/EmailFilled.svg"
							>
								Email
							</SocialLink>
							<SocialLink
								href="https://github.com/nerb-visage"
								image="/icons/Github.svg"
							>
								Github
							</SocialLink>
							<SocialLink
								href="https://gitlab.com/nerb-visage"
								image="/icons/Gitlab.svg"
							>
								Gitlab
							</SocialLink>
							<SocialLink
								href="https://www.linkedin.com/in/bren-steven-javier-137440250/"
								image="/icons/Linkedin.svg"
							>
								LinkedIn
							</SocialLink>
						</div>
					</div>
				</div>
				<div className="flex items-center lg:mt-20">
					<SideBar />
					<div
						className="md:mx-10 lg:mx-40 z-40 items-center flex
							text-gray-200 overflow-x-scroll"
					>
						<div
							className="w-full whitespace-nowrap overflow-auto scroll-smooth select-none snap-x no-scrollbar snap-always
							[&>*]:max-h-[600px] sm:[&>*]:max-h-[1000px] [&>*]:snap-start [&>*]:bg-violet-700/5 [&>*]:overflow-scroll [&>*]:touch-pan-auto
							lg:[&>*]:max-h-[400px] [&>div]:w-full [&>div]:inline-block [&>div]:whitespace-normal [&>div]:align-middle [&>div]:mr-20 text-sm
							"
						>
							<About />
							<Experience />
							<Technologies />
							<Projects />
						</div>
					</div>
				</div>
				<div className="mt-auto py-2 text-center bg-violet-800/20 text-sm">
					Built with <i className="italic">Next.js, </i>
					and <i className="italic">TailwindCSS</i>. Deployed with
					<b className="italic"> Vercel.</b>
				</div>
			</SpotlightHover>
		</main>
	);
}
