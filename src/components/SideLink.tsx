"use client";
import { useContext, useEffect, useRef, useState } from "react";
import { SpotlightCoordinatesContext } from "./Spotlight";
import { SIZE, FLASH_SIZE } from "@/constants";
import Image from "next/image";

export default function SideBar() {
	// To identify which link should be highlighted by getting the hash from the URL
	const [location, setLocation] = useState<string>("");

	useEffect(() => {
		setLocation(
			(typeof window !== "undefined" && window.location.hash) || "#about",
		);
	}, []);

	return (
		<div
			className="w-[400px] flex-shrink-0 h-full left-0 font-bold justify-center items-start pl-36 flex-col gap-y-12 z-40
				hidden md:flex"
		>
			<SideLink setLocation={setLocation} location={location} href="#about">
				About
			</SideLink>
			<SideLink
				setLocation={setLocation}
				location={location}
				href="#experience"
			>
				Experience
			</SideLink>
			<SideLink
				setLocation={setLocation}
				location={location}
				href="#technologies"
			>
				Technologies
			</SideLink>
			<SideLink setLocation={setLocation} location={location} href="#projects">
				Projects
			</SideLink>
		</div>
	);
}

function SideLink({
	children,
	href,
	location,
	setLocation,
}: {
	children: React.ReactNode;
	href: string;
	setLocation: (href: string) => void;
	location: string;
}) {
	return (
		<a
			onClick={() => setLocation(href)}
			href={href}
			draggable={false}
			className={`before:content-[''] before:transition-[width] before:duration-200 before:h-2 before:rounded-full before:bg-gray-400 before:relative before:z-0
				gap-4 flex items-center text-gray-400 select-none
				${
					location === href
						? "before:w-16 before:bg-white text-white"
						: "hover:before:w-8 hover:before:bg-white hover:text-white group before:w-2 "
				}`}
		>
			<SideLinkZoom>{children}</SideLinkZoom>
		</a>
	);
}

export function SocialLink({
	href,
	children,
	image,
}: {
	href: string;
	image: string;
	children: string;
}) {
	return (
		<a
			target="_blank"
			href={href}
			className="flex items-center md:px-4 px-2 py-2 gap-x-1 rounded-md select-none
				active:bg-violet-500/10 hover:bg-violet-500/10
				transition-colors duration-500 ease-in-out"
		>
			<Image
				className="aspect-square invert h-6"
				src={image}
				alt=""
				width={50}
				height={50}
			/>
			<span className="hidden md:block">{children}</span>
		</a>
	);
}

export function SideLinkZoom({ children }: { children: React.ReactNode }) {
	const { coordinates } = useContext(SpotlightCoordinatesContext);
	const [isHovering, setIsHovering] = useState(false);
	const ref = useRef<HTMLSpanElement>(null);
	const zoomRef = useRef<HTMLSpanElement>(null);

	function isHoveringOverLink(e: any) {
		setIsHovering(true);
	}

	return (
		<span
			className="flex justify-center items-center text-2xl"
			ref={ref}
			onClick={isHoveringOverLink}
			onMouseMove={isHoveringOverLink}
			onMouseOver={isHoveringOverLink}
			onMouseOut={(e) => {
				setIsHovering(false);
			}}
		>
			<span
				ref={zoomRef}
				className="text-3xl absolute z-10 border-red-500 text-violet-600"
				style={{
					opacity: isHovering ? 1 : 0,
					WebkitClipPath: `circle(${SIZE["xs"]}px at ${
						coordinates.x - (zoomRef.current?.getBoundingClientRect().left || 0)
					}px ${
						coordinates.y - (zoomRef.current?.getBoundingClientRect().top || 0)
					}px)`,
				}}
			>
				{children}
			</span>
			<span
				className="group-hover:px-7"
				style={{
					mask: isHovering
						? `radial-gradient(${SIZE["xs"] * 1.3}px at ${
								coordinates.x - (ref.current?.getBoundingClientRect().left || 0)
						  }px ${
								coordinates.y - (ref.current?.getBoundingClientRect().top || 0)
						  }px, transparent 0, transparent 98%, black 100%)`
						: "",
				}}
			>
				{children}
			</span>
		</span>
	);
}
