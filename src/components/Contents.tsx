import Image from "next/image";

export function About() {
	return (
		<div id="about" className="py-2 md:py-7 px-10 rounded-lg text-gray-200">
			<p className="my-4">
				Motivated Computer Science undergraduate from Cavite State University.
				Seeking to use learned skills in Web Development and Software
				Engineering. I am a fast learner who is always willing to learrn new
				things and apply them to my work.
			</p>
			<p className="my-4">
				Due to the pandemic, classes were held online asynchronously, which gave
				me a lot of free time that I spent mostly on learning new things and
				improving my skills. I spent most of my day during that time on online
				courses such as CS50, Freecodecamp, Youtube Tutorials, The Odin Project,
				and etc.. I also spent some time learning data structures and algorithms
				through solving LeetCode problems. In January of 2023, I joined a 5
				month online bootcamp
				<span className="font-semibold text-blue-400"> Village88 </span>
				which help me learn more about web development, especially on the
				frontend side.
			</p>
			<p></p>
		</div>
	);
}

export function Experience() {
	return (
		<div
			id="experience"
			className="py-2 md:py-7 px-10 rounded-lg max-h-[450px] overflow-scroll text-gray-200"
		>
			<div className="my-4">
				<h2 className="flex items-center italic">
					<span className="font-extrabold">
						Ascend Profit
						<span className="font-extrabold"> • </span>
						<span className="font-semibold">Web Developer Intern</span>
					</span>
				</h2>
				<ul className="ml-4 mt-2 font-normal [&>li]:my-2 [&>li]:list-disc pl-4">
					<li>
						Planned and Designed the backend for managing accounting entries for
						a SaaS web application using SalesForce.
					</li>
					<li>
						Developed a program for managing the inputs for the accounting
						entries using SalesForce Flow
					</li>
				</ul>
			</div>
			<div className="my-4">
				<h2 className="my-1">
					<span className="font-extrabold">
						Village88 Bootcamp
						<span className="font-extrabold"> • </span>
						<span className="font-semibold">Backend Development Track</span>
					</span>
				</h2>
				<ul className="ml-4 mt-2 font-normal [&>li]:my-2 [&>li]:list-disc pl-4">
					<li>
						Performed various different programming tasks and problems using
						different technologies such as HTML, CSS, Python, PHP, Javascript,
						Ruby, MySQL, and Docker.
					</li>
					<li>Developed web applications based on given specifications.</li>
					<li>
						Created an e-commerce website for a PHP capstone requirements in
						under a week.
					</li>
				</ul>
			</div>
		</div>
	);
}

export function Technologies() {
	return (
		<div id="technologies" className="py-7 px-2 md:px-10 rounded-lg">
			<div className="flex gap-5 justify-center items-center flex-wrap">
				<TechnologyBadge name="HTML" />
				<TechnologyBadge name="CSS" />
				<TechnologyBadge name="Javascript" />
				<TechnologyBadge name="JQuery" />
				<TechnologyBadge name="PHP" />
				<TechnologyBadge name="NodeJS" />
				<TechnologyBadge name="React" />
				<TechnologyBadge name="Typescript" />
				<TechnologyBadge name="NextJS" />
				<TechnologyBadge name="Tailwind" />
				<TechnologyBadge name="Bootstrap" />
				<TechnologyBadge name="Java" />
				<TechnologyBadge name="Kotlin" />
				<TechnologyBadge name="Python" />
				<TechnologyBadge name="Ruby" />
				<TechnologyBadge name="Ruby On Rails" />
				<TechnologyBadge name="PostgreSQL" />
				<TechnologyBadge name="MySQL" />
				<TechnologyBadge name="MongoDB" />
				<TechnologyBadge name="Bash" />
				<TechnologyBadge name="Linux" />
				<TechnologyBadge name="Docker" />
			</div>
		</div>
	);
}

function TechnologyBadge({ name }: { name: string }) {
	const iconName = name.replace(/\s/g, "");
	return (
		<div
			className="flex items-center gap-x-1 md:gap-x-2 font-semibold rounded-md bg-violet-700/25 w-fit
			text-sm sm:text-md py px-2 sm:py-2 sm:px-4 
			hover:bg-violet-700/80 transition-colors duration-500 ease-in"
		>
			<Image
				src={`/icons/Brand${iconName}.svg`}
				alt=""
				className="p-1 h-6 w-6 invert grayscale brightness-0"
				width={50}
				height={50}
			/>
			<span>{name}</span>
		</div>
	);
}
