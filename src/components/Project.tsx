"use client";
import { cn } from "@/helper";
import Image from "next/image";
import { ComponentProps, HTMLAttributes, useEffect, useState } from "react";
import { JsxAttribute } from "typescript";

export function Projects() {
	"use client";
	return (
		<div id="projects" className="rounded-lg p-2">
			<div className="flex flex-col gap-y-5">
				<ProjectCard
					image="/images/evo"
					title="EVO"
					description="An web application that allows users from CvSU State University
					to participate in elections, events and activities, and discussions from their organizations.
					Developed using HTML, TailwindCSS, Javascript, Express.js, JSX using @kitajs/html, HTMX, Postgres, and Prisma for Schema modeling"
				/>
				<ProjectCard
					image="/images/slapshtick"
					title="SlapStick"
					description="An ecommerce web application that allows secure online payment through
					credit cards via Stripe. Developed using HTML, CSS, JS, AJAX, Git, PHP,
					CodeIgniter, MySQL(MariaDB), and Stripe API."
					maxImageCount={5}
				/>
				<ProjectCard
					image="/images/elements"
					title="Elements.io"
					description="An android IO Game based on combining elements based on their atomic number. The game is
					built through Construct 2 engine – an HTML5 web based game engine for 2D game development
					via visual development."
					maxImageCount={4}
				/>
			</div>
		</div>
	);
}

function ProjectCard({
	title,
	description,
	image,
	maxImageCount,
}: {
	image: string;
	title: string;
	description: string;
	maxImageCount?: number;
}) {
	const [hovered, setHovered] = useState(false);

	return (
		<div
			onMouseOver={() => setHovered(true)}
			onMouseOut={() => setHovered(false)}
			className="rounded-md group p-2 flex gap-x-5 flex-1 shadow-md shadow-violet-950/50
				bg-violet-800/5 border-transparent border
				hover:shadow-lg hover:shadow-violet-950
				transition-all duration-500 ease-in-out relative"
		>
			<div
				className="absolute top-0 bottom-0 left-0 w-[1px] rounded-md bg-gradient-radial from-violet-800 via-transparent to-transparent opacity-0
					group-hover:opacity-100
					transition-all duration-500 ease-in-out"
			></div>
			<div
				className="absolute top-0 bottom-0 right-0 w-[1px] rounded-md bg-gradient-radial from-violet-800 via-transparent to-transparent opacity-0
					group-hover:opacity-100
					transition-all duration-500 ease-in-out"
			></div>
			<div className="w-32 aspect-square rounded-md bg-purple-400/40 overflow-clip">
				<ProjectCardImageSlideshow
					{...{ title, image, hovered, maxImageCount }}
				/>
			</div>
			<div className="flex-1">
				<h4 className="text-lg group-hover:text-violet-500 font-semibold">
					{title}
				</h4>
				<p className="text-md text-gray-300 group-hover:text-white">
					{description}
				</p>
			</div>
		</div>
	);
}

function ProjectCardImageSlideshow({
	title,
	image,
	hovered,
	maxImageCount = 3,
}: {
	image: string;
	title: string;
	hovered: boolean;
	maxImageCount?: number;
}) {
	const [currentImageCount, setCurrentImage] = useState(0);
	const [slideshowHandler, setSlideshowHandler] =
		useState<NodeJS.Timeout | null>(null);

	function setNewImage(count?: number) {
		setCurrentImage(
			(prevCount) =>
				count || (prevCount === maxImageCount - 1 ? 0 : prevCount + 1),
		);
		setSlideshowHandler(setTimeout(setNewImage, 2000));
	}
	useEffect(() => {
		if (hovered) {
			if (!slideshowHandler) setNewImage(1);
		} else {
			console.log("test");
			setCurrentImage(0);
			if (slideshowHandler) {
				clearTimeout(slideshowHandler);
				setSlideshowHandler(null);
			}
		}
		return () => {
			if (slideshowHandler) {
				clearTimeout(slideshowHandler);
				setSlideshowHandler(null);
			}
		};
	}, [hovered, currentImageCount]);

	return (
		<div className="relative w-32 h-32 rounded-md bg-purple-400/40 overflow-hidden flex justify-center">
			{hovered && (
				<ProjectCardImageView
					className="animate-fade-out"
					{...{
						image,
						title,
						currentImageCount:
							currentImageCount === 0
								? maxImageCount - 1
								: currentImageCount - 1,
					}}
				/>
			)}
			<ProjectCardImageView {...{ image, title, currentImageCount }} />
		</div>
	);
}

export const ProjectCardImageView: React.FunctionComponent<{
	className?: string;
	image: string;
	currentImageCount: number;
	title: string;
}> = function ({ image, currentImageCount, title, className }) {
	useEffect(() => {}, [currentImageCount]);
	return (
		<Image
			key={`${image}-${currentImageCount}`}
			width={1024}
			height={720}
			src={`${image}-${currentImageCount}.jpg`}
			className={cn(
				"object-cover h-full group-hover:scale-105 w-auto transition-transform animate-fade-in absolute top-0",
				className,
			)}
			alt={`Preview Image for ${title}`}
		/>
	);
};
