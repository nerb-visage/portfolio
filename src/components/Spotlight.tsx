'use client'
import { createContext, useContext, useEffect, useRef, useState } from 'react';
import { SIZE, FLASH_SIZE } from '@/constants';

export const SpotlightCoordinatesContext = createContext({ coordinates: { x: 0, y: 0 }, setZoom: (value: boolean) => { } })

function SpotlightBackground({ x, y }: { x: number, y: number }) {
	return <span
		className={`sm:hidden lg:block absolute from-violet-900/20 via-violet-950/0 to-violet-900/0 bg-gradient-radial z-[-1]`}
		style={{
			width: `${FLASH_SIZE}px`,
			height: `${FLASH_SIZE}px`,
			left: `${x - (FLASH_SIZE / 2)}px`,
			top: `${y - (FLASH_SIZE / 2)}px`
		}}
	/>
}

function SpotlightBall({
	zoom,
	x,
	y,
	className
}: { zoom: boolean, x: number, y: number, className?: string }) {
	return <span
		draggable={false}
		className={`absolute hidden lg:block clip backdrop-invert opacity-100 rounded-full z-20 select-none
			shadow-violet-950/25 shadow-lg transition-[transform,mask] duration-150 ease-linear ${zoom && "scale-125"} ${className}`}
		style={{
			height: `${SIZE["sm"]}px`,
			width: `${SIZE["sm"]}px`,
			left: `${x - (SIZE["sm"] / 2)}px`,
			top: `${y - (SIZE["sm"] / 2)}px`,
			mask: `radial-gradient(${zoom ? "20px" : "15px"} at center center, transparent 0, transparent 95%, black 100%)`,
		}}>
	</span>
}

export function SpotlightHover({ children }: { children: React.ReactNode }) {
	const [coordinates, setCoordinates] = useState({ x: 0, y: 0 });
	const [ballZoom, setZoom] = useState(false)

	useEffect(() => {
		document.body.onmousemove = (e) => {
			setCoordinates({ x: e.pageX, y: e.pageY })
		}
	})

	return (
		<div className='h-full w-full flex-1 flex-col flex'>
			<SpotlightCoordinatesContext.Provider value={{ coordinates, setZoom }}>
				<div className='overflow-hidden absolute h-full w-full flex-1'>
					<SpotlightBackground {...coordinates} />
				</div>
				{children}
			</SpotlightCoordinatesContext.Provider>
		</div>
	)
}

