export const SIZE = {
  xs: 25,
  sm: 45,
  md: 60,
} as const;

export const FLASH_SIZE = 1200 as const;

